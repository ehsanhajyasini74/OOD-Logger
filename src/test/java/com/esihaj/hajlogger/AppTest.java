package com.esihaj.hajlogger;

import com.esihaj.hajlogger.appender.Appender;
import com.esihaj.hajlogger.layout.JsonLayout;
import com.esihaj.hajlogger.layout.Layout;
import com.esihaj.hajlogger.layout.PlainLayout;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    Appender stdAppender, fileAppender;

    public AppTest( String testName )
    {
        super( testName );
        try {
            Layout layout = new JsonLayout();
            this.stdAppender = LogCTX.getPlainStdoutAppender();
            this.fileAppender = LogCTX.getPlainFileAppender("app.log");
            stdAppender.setLayout(layout);
            LogFilter filter = new LogFilter() {
                public boolean canShow(LogEntry entry) {
                    return entry.getLevel().getLevelValue() >= Level.INFO.getLevelValue();
                }
            };
            stdAppender.addFilter(filter);
            Logger logger = new Logger(stdAppender, LogCTX.DEFAULT_SCOPE);
            LogCTX.setRootLogger(logger);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    private void doRoutine() {
        Logger root = LogCTX.getRootLogger();
        root.Error("oh my god! error 1");
        Logger forLogger = root.getLogger("for loop", Level.INFO);
        for (int i = 0; i < 3; i++) {
            forLogger.Log("logging No." + i);
        }
        root.getAppender().clearFilters();
        forLogger.Warn("for finished!");
        root.Debug("root finished!");
    }
    public void testFile()
    {
        System.out.println();
        LogCTX.getRootLogger().setAppender(this.fileAppender);
        doRoutine();
    }

    public void testStdout()
    {
        System.out.println();
        LogCTX.getRootLogger().setAppender(this.stdAppender);
        doRoutine();
    }

    public void testLayout()
    {
        System.out.println();
        LogCTX.getRootLogger().setAppender(this.stdAppender);
        LogCTX.getRootLogger().getAppender().setLayout(new PlainLayout().disableLevel().showDate("hh:mm::ss"));
        doRoutine();
    }

    public void testFilters(){
        System.out.println();
        LogFilter levelFilter = new LogFilter() {
            public boolean canShow(LogEntry entry) {
                return entry.getLevel().getLevelValue() >= Level.WARN.getLevelValue();
            }
        };
        LogFilter badMsgFilter = new LogFilter() {
            public boolean canShow(LogEntry entry) {
                return !entry.getMsg().contains("bad msg");
            }
        };

        Logger root = LogCTX.getRootLogger();
        root.getAppender().clearFilters();
        root.getAppender().addFilter(badMsgFilter);

        root.Error("oh my god! error 1");
        root.Error("oh my god! bad msg [invisible] error 2");

        Logger forLogger = root.getLogger("for loop", Level.INFO);
        forLogger.addFilter(levelFilter);

        for (int i = 0; i < 3; i++) {
            forLogger.Log("logging No." + i);
        }
        forLogger.Warn("bad msg [invisible] for finished");
        root.getAppender().clearFilters();
        forLogger.Warn("for finished! bad msg [visible]");
        root.Debug("root finished!");

    }
}
