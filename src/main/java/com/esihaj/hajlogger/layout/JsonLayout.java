package com.esihaj.hajlogger.layout;

import com.esihaj.hajlogger.LogEntry;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by esihaj on 2/27/17.
 */
public class JsonLayout implements Layout {
    Gson gson = new Gson();

    public String toString(LogEntry entry) {
        return gson.toJson(entry).toString();
    }

}