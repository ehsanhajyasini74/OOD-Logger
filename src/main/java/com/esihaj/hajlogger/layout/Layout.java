package com.esihaj.hajlogger.layout;

import com.esihaj.hajlogger.LogEntry;

/**
 * Created by esihaj on 2/26/17.
 */
public interface Layout {
    String toString(LogEntry entry);
}
