package com.esihaj.hajlogger.layout;

import com.esihaj.hajlogger.LogEntry;
import org.omg.CORBA.PUBLIC_MEMBER;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;

/**
 * Created by esihaj on 2/27/17.
 */
public class PlainLayout implements Layout {

    public static final String SCOPE = "scope";
    public static final String DATE = "date";
    public static final String LEVEL = "level";
    public static final String MSG = "msg";
    public static final String SEPERATOR = " - ";

    protected String dateFormat = "MM/dd/yyyy - hh:mm:ss";
    protected HashMap<String, Boolean> showFields = new HashMap<String, Boolean>() {{
        put(SCOPE, true);
        put(DATE, true);
        put(LEVEL, true);
        put(MSG, true);
    }};

//    {
//        showFields.put("scope", true);
//        showFields.put("date", true);
//        showFields.put("level", true);
//        showFields.put("msg", true);
//    }

    public String toString(LogEntry entry) {
        StringBuilder sb = new StringBuilder();
        if(showFields.get(SCOPE)) {
            sb.append("[" + entry.getScope() + "]");
            sb.append(SEPERATOR);
        }
        if(showFields.get(DATE)) {
            DateFormat df = new SimpleDateFormat(dateFormat);
            sb.append(df.format(entry.getDate()));
            sb.append(SEPERATOR);
        }
        if(showFields.get(LEVEL)) {
            sb.append(entry.getLevel().getName());
            sb.append(SEPERATOR);
        }
        if(showFields.get(MSG)) {
            sb.append(entry.getMsg());
        }

        return sb.toString();
    }

    public PlainLayout showDate(String dateFormat) {
        showFields.put(DATE, true);
        this.dateFormat = dateFormat;
        return this;
    }

    public PlainLayout enableMsg() {
        showFields.put(MSG, true);
        return this;
    }

    public PlainLayout enableLevel() {
        showFields.put(LEVEL, true);
        return this;
    }

    public PlainLayout enableScope() {
        showFields.put(SCOPE, true);
        return this;
    }

    public PlainLayout disableMsg() {
        showFields.put(MSG, false);
        return this;
    }

    public PlainLayout disableLevel() {
        showFields.put(LEVEL, false);
        return this;
    }

    public PlainLayout disableScope() {
        showFields.put(SCOPE, false);
        return this;
    }
}
