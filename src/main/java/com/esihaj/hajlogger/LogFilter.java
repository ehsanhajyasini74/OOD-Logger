package com.esihaj.hajlogger;

/**
 * Created by esihaj on 2/27/17.
 */
public interface LogFilter {
    boolean canShow(LogEntry entry);
}
