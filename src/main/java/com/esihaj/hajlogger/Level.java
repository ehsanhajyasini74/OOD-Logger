package com.esihaj.hajlogger;

/**
 * Created by esihaj on 2/27/17.
 */
public class Level {
    public static final LogLevel ERROR = new LogLevel("ERROR", 400);
    public static final LogLevel WARN = new LogLevel("WARNING", 300);
    public static final LogLevel INFO = new LogLevel("INFO", 200);
    public static final LogLevel DEBUG = new LogLevel("DEBUG", 100);
    public static final LogLevel ALL = new LogLevel("ALL", 0);
}
