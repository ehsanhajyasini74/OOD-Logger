package com.esihaj.hajlogger.appender;

import com.esihaj.hajlogger.layout.Layout;
import com.esihaj.hajlogger.LogEntry;
import com.esihaj.hajlogger.LogFilter;

import java.util.ArrayList;

/**
 * Created by esihaj on 2/26/17.
 */
public abstract class Appender {
    protected Layout layout;
    protected ArrayList<LogFilter> filters = new ArrayList<LogFilter>();

    public Appender(Layout layout) {
        this.layout = layout;
    }

    public void add(LogEntry entry){
        for (LogFilter filter: filters) {
            if(!filter.canShow(entry))
                return; // can't show
        }
        process(entry);
    }

    protected abstract void process(LogEntry entry);

    public void addFilter(LogFilter f) {
        filters.add(f);
    }

    public void clearFilters() {
        filters.clear();
    }

    public Layout getLayout() {
        return layout;
    }

    public void setLayout(Layout layout) {
        this.layout = layout;
    }
}
