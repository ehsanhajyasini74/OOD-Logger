package com.esihaj.hajlogger.appender;

import com.esihaj.hajlogger.LogEntry;
import com.esihaj.hajlogger.layout.Layout;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * Created by esihaj on 2/27/17.
 */
public class WriterAppender extends Appender {
    PrintWriter writer;

    public WriterAppender(Layout layout, PrintWriter writer) {
        super(layout);
        this.writer = writer;
    }

    @Override
    protected void process(LogEntry entry) {
        writer.println(layout.toString(entry));
        writer.flush();
    }
}
