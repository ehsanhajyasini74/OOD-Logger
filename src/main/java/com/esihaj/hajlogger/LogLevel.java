package com.esihaj.hajlogger;

/**
 * Created by esihaj on 2/26/17.
 */
public class LogLevel {
    String name;
    int levelValue;

    public LogLevel(String name, int levelValue) {
        this.levelValue = levelValue;
        this.name = name;
    }

    public int getLevelValue() {
        return levelValue;
    }

    public String getName() {
        return name;
    }
}
