package com.esihaj.hajlogger;

import java.util.Date;

/**
 * Created by esihaj on 2/26/17.
 */
public class LogEntry {
    String msg;
    String scope;
    Date date;
    LogLevel level;

    public LogEntry(String msg, String scope, Date date, LogLevel level) {
        this.msg = msg;
        this.scope = scope;
        this.date = date;
        this.level = level;
    }

    public LogEntry(String msg, String scope, LogLevel level) {
        this.msg = msg;
        this.scope = scope;
        this.date = new Date();
        this.level = level;
    }

    public String getMsg() {
        return msg;
    }

    public String getScope() {
        return scope;
    }

    public Date getDate() {
        return date;
    }

    public LogLevel getLevel() {
        return level;
    }
}
