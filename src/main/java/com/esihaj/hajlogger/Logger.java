package com.esihaj.hajlogger;

import com.esihaj.hajlogger.appender.Appender;

import java.util.ArrayList;

/**
 * Created by esihaj on 2/26/17.
 */
public class Logger {
    protected Appender appender;
    protected LogLevel currentLevel = Level.ALL;
    protected String scopeName = "";
    protected ArrayList<LogFilter> filters = new ArrayList<LogFilter>();

    public Logger(Appender appender) {
        this.appender = appender;
    }

    public Logger(Appender appender, String scopeName) {
        this.appender = appender;
        this.scopeName = scopeName;
        this.currentLevel = Level.DEBUG;
    }

    //Get new logger for the new scope
    public Logger getLogger(String scopeName, LogLevel level){
        Logger newLogger = new Logger(this.appender, scopeName);
        newLogger.currentLevel = level;
        return newLogger;
    }

    //Appenders
    public Appender getAppender() {
        return appender;
    }

    public void setAppender(Appender appender) {
        this.appender = appender;
    }

    //Filters
    public void addFilter(LogFilter f) {
        filters.add(f);
    }

    public void clearFilters() {
        filters.clear();
    }

    //Log methods
    public void Log(String msg, LogLevel level) {
        LogEntry entry = new LogEntry(msg, scopeName, level);
        for (LogFilter filter: filters) {
            if(!filter.canShow(entry))
                return; // can't show
        }
        appender.add(entry);
    }

    public void Log(String msg) {
        Log(msg, currentLevel);
    }

    public void Error(String msg) {
        Log(msg, Level.ERROR);
    }

    public void Warn(String msg) {
        Log(msg, Level.WARN);
    }

    public void Info(String msg) {
        Log(msg, Level.INFO);
    }

    public void Debug(String msg) {
        Log(msg, Level.DEBUG);
    }

}
