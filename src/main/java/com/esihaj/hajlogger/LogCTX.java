package com.esihaj.hajlogger;

import com.esihaj.hajlogger.appender.Appender;
import com.esihaj.hajlogger.appender.WriterAppender;
import com.esihaj.hajlogger.layout.Layout;
import com.esihaj.hajlogger.layout.PlainLayout;

import java.io.*;

/**
 * Created by esihaj on 2/27/17.
 */
public class LogCTX {
    public static final String DEFAULT_SCOPE = "ROOT";
    private static Logger rootLogger;

    public static void initRootLogger() throws UnsupportedEncodingException{
        rootLogger = new Logger(getPlainStdoutAppender(), DEFAULT_SCOPE);
    }

    public static Logger getRootLogger() {
        return rootLogger;
    }

    public static void setRootLogger(Logger logger){
        rootLogger = logger;
    }

    public static Appender getPlainStdoutAppender() throws UnsupportedEncodingException {
        Layout layout = new PlainLayout();
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(System.out, "UTF-8"));
        return new WriterAppender(layout, writer);
    }

    public static Appender getPlainFileAppender(String fileName) throws FileNotFoundException {
        Layout layout = new PlainLayout();
        PrintWriter writer = new PrintWriter(fileName);
        return new WriterAppender(layout, writer);
    }
}

